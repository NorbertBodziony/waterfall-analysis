import collections
import functools

Investor = collections.namedtuple('Investor', 'shares invested type')

shareholders = {
    0: Investor(1000000, 0, 'Common'),  # Common
    1: Investor(200000, 900000, 'Series A'),  # Series A
    2: Investor(300000, 2100000, 'Series B'),  # Series B
    3: Investor(1500000, 15000000, 'Series C'),  # Serires C
}
allShares = functools.reduce(
    lambda sum, x: sum+x.shares, shareholders.values(), 0)
allInvested = functools.reduce(
    lambda sum, x: sum+x.invested, shareholders.values(), 0)

# print(allShares)
# print(allInvest)

PREFERENCE = 1
CAP = 2
caped = {0}
returns = [0, 0, 0, 0]

def prefOrCommon(investor, moneyLeft, sharesLeft):
    commonReturn = investor.shares/sharesLeft*moneyLeft
    if(investor.invested + investor.shares /
            sharesLeft*(moneyLeft-investor.invested) > investor.invested*CAP):
        prefReturn = investor.invested*CAP
    else:
        prefReturn = investor.invested + investor.shares / \
            sharesLeft*(moneyLeft-investor.invested)

    # print(f' pref amount {prefReturn}')
    if(commonReturn > investor.invested*CAP):
        return ('common')
    elif(prefReturn == commonReturn):
        return('common')
    else:
        return('pref')


def willHitCap(investor, prefValue, commonValue):
    if(prefValue+commonValue > investor.invested*CAP):
        return True
    else:
        return False



def splitCommon(leftShares, leftAmount):
    for key in shareholders:
        if(key not in caped and returns[key] != 0 and willHitCap(shareholders[key], returns[key], shareholders[key].shares / leftShares*(leftAmount))):
            returns[key] = shareholders[key].invested*CAP
            leftShares -= shareholders[key].shares
            leftAmount -= shareholders[key].invested
            caped.add(key)
            return splitCommon(leftShares, leftAmount)
    for key in shareholders:
        if(key == 0 or key not in caped):
            returns[key] += shareholders[key].shares / leftShares*(leftAmount)


def liquidation(amount=60000000):
    leftAmount = amount
    leftShares = allShares
    if(leftAmount < allInvested):
        for key in reversed(list(shareholders.keys())):
            if(shareholders[key].invested >= leftAmount):
                returns[key] = leftAmount
                break
            else:
                returns[key] = shareholders[key].invested
                leftAmount -= returns[key]
        for key in shareholders:
            print(
                f'{shareholders[key].type} : ${returns[key]:.0f} ')
        print(f'Exit Value {amount/1000000} M')

    else:
        for index in reversed(list(shareholders.keys())):
            sharesType = prefOrCommon(
                shareholders[index], leftAmount, leftShares)
            if(sharesType == 'pref'):
                leftAmount -= shareholders[index].invested
                returns[index] = shareholders[index].invested
        prefReturns = returns.copy()
        splitCommon(leftShares, leftAmount)
        # allsum = functools.reduce(lambda sum, x: sum+x, returns, 0)
        for key in shareholders:
            print(
                f'{shareholders[key].type} : ${returns[key]:.0f} Pref : ${prefReturns[key]:.0f} Common: ${returns[key]-prefReturns[key]:.0f} {"Caped" if key!=0 and key in caped else "" }')
        print(f'Exit Value {amount/1000000} M')


liquidation(45000000)
